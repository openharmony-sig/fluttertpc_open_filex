/*
* Copyright (c) 2024 SwanLink (Jiangsu) Technology Development Co., LTD.
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

export const OHOS_WANT_ACTION_VIEW_DATA: string = 'ohos.want.action.viewData';

export const OHOS_WANT_ACTION_SEND_DATA: string = 'ohos.want.action.sendData';

export const HARMONY_OS_PHOTOS_BUNDLE_NAME: string = 'com.huawei.hmos.photos';

export const HARMONY_OS_PHOTOS_ABILITY_NAME: string = 'com.huawei.hmos.photos.MainAbility';

export const OPEN_HARMONY_PHOTOS_BUNDLE_NAME: string = 'com.ohos.photos';

export const OPEN_HARMONY_PHOTOS_ABILITY_NAME: string = 'com.ohos.photos.MainAbility';

export const FILE_PROTOCOL_HEADER: string = 'file://';

export const PHOTO_PROTOCOL_HEADER: string = 'file://media/Photo/';

export const CAN_NOT_MATCH_ANY_COMPONENT: number = 16000019;
